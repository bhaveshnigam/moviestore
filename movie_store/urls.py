from django.conf.urls import patterns, include, url
from django.core.urlresolvers import reverse_lazy
from django.views.generic.base import RedirectView

from django.contrib import admin


admin.autodiscover()


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
	url(r'^movies/', include('movies.urls')),
	url(r'^', RedirectView.as_view(url=reverse_lazy('movie_list'))),
)
