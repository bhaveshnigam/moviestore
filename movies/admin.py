from django.contrib import admin
from movies.models import Studio, Genre, Movie


class StudioAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}

class GenreAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}


class MovieAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}

admin.site.register(Studio, StudioAdmin)
admin.site.register(Genre, GenreAdmin)
admin.site.register(Movie, MovieAdmin)