from django.conf.urls import patterns, url, include
from django.views.generic import ListView, DetailView
from movies.models import Genre, Movie, Studio


urlpatterns = patterns('',
  url('^genres/(?P<slug>[-\w]+)/$', DetailView.as_view(model=Genre),name='movie_genre_detail'),
  url ('^genres/$', ListView.as_view(model=Genre),name='movie_genre_list'),
  url('^studios/(?P<slug>[-\w]+)/$', DetailView.as_view(model=Studio), name='movie_studio_detail'),
  url ('^studios/$',ListView.as_view(model=Studio),name='movie_studio_list'),
  url('^(?P<slug>[-\w]+)/$', DetailView.as_view(model=Movie),name='movie_detail'),
  url ('^$',ListView.as_view(model=Movie),name='movie_list'),
)
